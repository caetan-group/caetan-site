# Migrate your git repository on GitLab

The first thing you want to do is prepare your git repository on GitLab to host the Markdown sources for your documentation [according to MkDocs](https://www.mkdocs.org/user-guide/writing-your-docs/).

#### A. Prepare the git repository

There are a couple of ways to go about this:

1. You can create a new git repository altogether and start from scratch. You can achieve that by explicitly following the ["Create a new git repository on GitLab" step](../new/new_repository.md) from the [New documentation how-to guide](../new/index.md). Please keep in mind that doing this implies you will have to manually make sure the configuration of your new git repository is similar to the one of your existing git repository.
2. You can work on your existing git repository directly in the current branch (presumambly the "master" branch) or, even better, in a new branch. This how-to guide will not go into more details about [git branching](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell) etc, it's assumed that you are comfortable enought with those concepts, given that you are reading this part of the how-to guide.

#### B. Prepare the Markdown sources and MkDocs configuration

##### Migrating from MkDocs

If your existing documentation is already using MkDocs, you're in luck!

There isn't much to do except possibly clean up deprecated files from your existing repository. For example, you might have been using GitLab CI with a `.gitlab-ci.yml` file to build and deploy your documentation. This will no longer be needed as OpenShift will be building and deploying your documentation.

##### Migrating from GitBook

If your existing documentation is using [GitBook](https://github.com/GitbookIO/gitbook-cli), you're still relatively in luck.

Same as MkDocs, GitBook allows you to organise your Markdown source files in standard directories and files according to your preferences. A configuration file is then used to define the structure and navigation of your documentation (`summary.md` in the case of GitBook and `mkdocs.yml` in the case of MkDocs).

To make the transition from GitBook to MkDocs easier, you can use [this simple Gitbook to MkDocs converter](https://preflight.web.cern.ch/markdown/gitbook_to_mkdocs) that takes in some standard metadata and your `summary.md` file and produces an `mkdocs.yml` file.

Finally, you possibly want to clean up deprecated files from your existing repository. For example, you might have been using GitLab CI with a `.gitlab-ci.yml` file to build and deploy your documentation. This will no longer be needed as OpenShift will be building and deploying your documentation.

##### Migrating from others

If your existing documentation is already using Markdown it should likely be easy to migrate it to MkDocs, as long as you follow the [MkDocs user guide](https://www.mkdocs.org/user-guide/writing-your-docs/) to structure your sources and write your configuration.

In all other cases you will most likely need to convert your sources to Markdown and also follow the [MkDocs user guide](https://www.mkdocs.org/user-guide/writing-your-docs/) to structure your sources and write your configuration.

#### C. Verify that everything is in order

By now you should have a git repository with a minimum structure that looks like this:

```
mkdocs.yml
docs/
    index.md
```

* `mkdocs.yml` is the MkDocs [configuration](https://www.mkdocs.org/user-guide/configuration/) file.
* `docs` is the default documentation directory where all your source Markdown files should live.
* `index.md` is, by convention, the default homepage of your documentation.

For more advanced suggestions on how to customise MkDocs, please have a look at this [advanced topic](../advanced/mkdocs.md).

When you are ready you can move on to the next step and [migrate your site on Web Services](migrate_site.md).
