# Customise your documentation URL

Now that your documentation has been deployed, you can customise its URL.

By default, the URL is based on the site name your chose when you [created your site on Web Services](migrate_site.md) (for example `http://service-docs.web.cern.ch`). To make your URL more recognisable as a documentation site you can use the `.docs.cern.ch` domain (for example `http://service.docs.cern.ch`).

!!! tip "Keep in mind"
    If keeping your existing site URL is important make sure you customise it accordingly in this step.
    
You can additionally make your documentation site more secure. Most modern browsers will nowadays flag web sites as not secure if they are not served with the **HTTPS** protocol. OpenShift serves it by default with the **HTTP** protocol (`http://service.docs.cern.ch`). You can very easily have it served with **HTTPS** (`https://service.docs.cern.ch`).

#### A. Go to your OpenShift project

![Screenshot](/images/openshift-project-deployed.png)

---

#### B. Go to your OpenShift project Routes

Hover over **Applications** on the left and click on **Routes**.

![Screenshot](/images/openshift-project-routes-do.png)

---

#### C. Go to your documentation route

Click on **docs** from the list of routes. Unless you have a more complicated set-up you should only have one route in the list. The name **docs** might be different for you and it's essentially the **application name** you provided when you first [selected MkDocs and deployed your documentation](deploy.md#e-fill-in-the-information).

![Screenshot](/images/openshift-project-routes-route-do.png)

---

#### D. Edit your documentation route

Click on the **Actions** menu at the upper right corner and click on **Edit**.

![Screenshot](/images/openshift-project-routes-route-edit-do.png)

---

![Screenshot](/images/openshift-project-routes-route-edit.png)

---

#### E. Customise the hostname

* **Hostname**: Enter the hostname for your documenation site:
    * If you want to use the `.docs.cern.ch` domain enter `service.docs.cern.ch`, for example.
    * If you want to keep your existing site URL leave it unchanged. For example `service-docs.web.cern.ch`.

![Screenshot](/images/openshift-project-routes-route-edit-filled_in-hostname.png)

---

#### F. Enable the secure route

* Check the **Secure route** checkbox <input type="checkbox" checked="checked">.
* Select the "Redirect" option for **Insecure Traffic**.

![Screenshot](/images/openshift-project-routes-route-edit-filled_in-secure.png)

---

#### G. Click on Save

Click on **Save** at the bottom of the page.

![Screenshot](/images/openshift-project-routes-route-edit-filled_in-do.png)

---

Your documentation URL is now customised and it is served with **HTTPS**. You can move on to the next step to [make sure your documentation is automatically redeployed every time you update your git repository](redeploy.md).
