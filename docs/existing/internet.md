# Make your service documentation available outside of CERN

It's probable that you want your service documentation to be available outside of the CERN network, essentially the entire Internet (such that you can easily access it from home, as well as your colleagues that are possibly outside of CERN). If this is not the case, then please feel free to skip this step and move on to the final step and [go <span style="color: green;">green</span>!](green.md)

When you [created your site](migrate_site.md) Web Services made it available by default to the internal CERN network only. You can easily make it available to the entire Internet.

!!! note
    If you want to make your service documentation available only to specific people/groups at CERN it's best that you have a look at this [advanced topic](../advanced/authentication_authorisation.md) for more information on authentication and authorisation options.

#### A. Go to your site management on Web Services

Go to the [My websites](https://webservices.web.cern.ch/webservices/Services/ManageSite/) page and click on your site under "My websites" on the left. For example on `http://cern.ch/service-docs` (replace *service* by the actual name of your service).

![Screenshot](/images/web_services-my_websites-do.png)

---

![Screenshot](/images/web_services-site_management.png)

---

#### B. Go to your **Site Access & Permissions** configuration

Click on **Site Access & Permissions** under "Toolbox for current site" on the left.

![Screenshot](/images/web_services-site_management-site_access_permissions-do.png)

---

#### C. Select the Internet visibility setting

Select the "Internet" option for **visibility setting**.

![Screenshot](/images/web_services-site_management-site_access_permissions.png)

---

Your documentation is now available to the entire Internet and you can move on to the final step and [go <span style="color: green;">green</span>!](green.md)
