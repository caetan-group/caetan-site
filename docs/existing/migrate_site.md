# Migrate your site on Web Services

Once the Markdown sources for your documentation are prepared in your git repository according to MkDocs you want to prepare your site on Web Services.

#### A. Prepare the site

##### Migrating from a PaaS Web Application

If the type of your existing site is "PaaS Web Application" (i.e. on OpenShift), you're in luck!

There isn't much to do except possibly clean up any existing configuration in your OpenShift project that you won't need any longer after [deploying your documentation with MkDocs](../existing/deploy.md). This how-to guide will not go into more details about [managing your OpenShift project](https://cern.service-now.com/service-portal/article.do?n=KB0004358) etc, it's assumed that you are comfortable enought with those concepts, given that you are reading this part of the how-to guide.

##### Migrating from an EOS folder

If the type of your existing site is "EOS folder" (i.e. on WebEOS), there are a couple of ways to go about it:

1. If you don't want to keep your existing site URL, you can create a new site altogether and start from scratch. You can achieve that by explicitly following the ["Create a new site on Web Services" step](../new/new_site.md) from the [New documentation how-to guide](../new/index.md).
2. If keeping your existing site URL is important, you can first delete your existing site, wait for DNS to refresh and then create a new site with the same site name as the existing one by explicitly following the ["Create a new site on Web Services" step](../new/new_site.md) from the [New documentation how-to guide](../new/index.md).

!!! tip "Keep in mind"
    It takes about 1 hour to delete a site, wait for DNS to refresh and create a new site. If you can't afford to have an 1 hour downtime, please get in touch with the [CERN Service Portal](https://cern.service-now.com/service-portal/function.do?name=web-infrastructure) to ask for alterntive migration options.

!!! note
    If keeping your existing site URL is important it is likely that you also want to keep all your documentation links the same. This depends on how URLs are formed with your existing documentation compared to how URLs are formed with MkDocs. You can provide a custom configuration for the Nginx server that will be serving your documentation in your OpenShift project to redirect and rewrite URLs as you see fit. Please have a look at the corresponding [advanced topic](../advanced/nginx.md) for more information. In particular, if you are **migrating from GitBook on WebEOS to MkDocs on OpenShift**, have a look at [this advanced topic](../advanced/gitbook_on_webeos_to_mkdocs_on_openshift.md) on how to keep all your documentation links the same.

#### B. Verify that everything is in order

By now you should have a site of type "PaaS Web Application" ready to host your documentation. If you created a new site it will have taken a few minutes to be created and you will have been notified by e-mail.

You can now go ahead and [generate a deploy token for your git repository](deploy_token.md) which you will later use to make sure OpenShift has access to it.
