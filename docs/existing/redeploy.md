# Automatically redeploy your documentation every time you update your git repository

Now that your documentation has been deployed and is available on the web, you have to make sure it's automatically redeployed in the future, every time you update your Markdown sources and/or MkDocs configuration in your git repository.

#### A. Go to your OpenShift project

![Screenshot](/images/openshift-project-deployed-customised_URL.png)

---

#### B. Go to your OpenShift project Builds

Hover over **Builds** on the left and click on **Builds**.

![Screenshot](/images/openshift-project-builds-do.png)

---

#### C. Go to your documentation build

Click on **docs** from the list of builds. Unless you have a more complicated set-up you should only have one build in the list. The name **docs** might be different for you and it's essentially the **application name** you provided when you first [selected MkDocs and deployed your documentation](deploy.md#e-fill-in-the-information).

![Screenshot](/images/openshift-project-builds-build-do.png)

---

![Screenshot](/images/openshift-project-builds-build.png)

---

#### D. Go to your documentation build's configuration

Click on the **Configuration** tab.

![Screenshot](/images/openshift-project-builds-build-configuration-do.png)

---

#### E. Get the **Generic Webhook URL**

Carefully copy the entire value of the **Generic Webhook URL**. It should start with `https://openshift.cern.ch` and end with `generic`.

![Screenshot](/images/openshift-project-builds-build-configuration-webhook-copy.png)

---

#### F. Go to your git repository on GitLab

Visit the URL of your git repository (for example `https://gitlab.cern.ch/service/docs`).

#### G. Go to your git repository's **Webhooks Settings**

Hover over **Settings** on the left and click on **Webhooks**.

![Screenshot](/images/gitlab-settings_integrations-do-1.png)

---

#### H. Fill in the information

* **URL**: Enter the **Generic Webhook URL** (the entire value of the **Generic Webhook URL** you previously copied from OpenShift).
* **Secret Token**: Leave empty.
* **Push events**: Leave checked <input type="checkbox" checked="checked">.
    * **Branch name or wildcard pattern to trigger on (leave blank for all)**: Enter "master". This is by default the main branch name for git repositories. This way the webhook URL will be triggered only when the "master" branch is updated. Unless you are planning on using a more complicated set-up and you know what you're doing we suggest you enter "master".
* Leave all other **Triggers** unchecked <input type="checkbox">.
* **Enable SSL verification**: Leave checked <input type="checkbox" checked="checked">.

![Screenshot](/images/gitlab-settings_integrations-filled_in-1.png)

---

#### I. Click on **Add webhook**

![Screenshot](/images/gitlab-settings_integrations-filled_in-do.png)

---

More information on this process can be found at the [respective OpenShift Knowledge Base article](https://cern.service-now.com/service-portal/article.do?n=KB0004498).

Your service documentation will now be automatically redeployed every time you update your git repository. Please go ahead with the next step and [make your documentation available outside of CERN](internet.md).
