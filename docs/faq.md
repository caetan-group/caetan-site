Frequently Asked Questions
==========================

Q1: Why do I get a _403_ error when I'm connecting to https://myservice.docs.cern.ch which I have just deployed?

A1: You must have an _index.md_ file in your _docs_ directory for your documentation to be displayed. More info [here](https://www.mkdocs.org/user-guide/writing-your-docs/#index-pages).

Q2: Can I import existing Markdown documentation from GitHub and repositories?

A2: Yes, GitLab allows to sync GitHub repos. You could make use of git _submodules_ to achieve import.

Q3: How to make LaTeX work in a MkDocs documentation?

A3: See [this dedicated page](advanced/latex.md) explaining what to add.

Q4: I need some help with Markdown syntax, especially _nested lists_.

A4: Have a look at [https://wordpress.com/support/markdown-quick-reference/](https://wordpress.com/support/markdown-quick-reference/) and  [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#lists](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#lists) and [https://daringfireball.net/projects/markdown/syntax](https://daringfireball.net/projects/markdown/syntax)

Q5: Why sometimes changes in .md files are not visible?

A5: Your _mkdocs.yml_ file might be incorrect. Indentation in YAML is very important. You can use an online tool like [https://codebeautify.org/yaml-validator](https://codebeautify.org/yaml-validator) to validate the mkdocs.yml contents.

Q6: I am trying to clone my GitLab repository from which my .docs.cern.ch documentation is generated but I get an error.

A6: This is probably because you are using `https` as the clone method and the project is not public. The project can be only cloned by authenticated users. You can either provide the authentication credentials or just make the project public so that it can be cloned.

Q7: May I give different access rights for different files or subdirectories of my .docs.cern.ch site, if possible based on e-groups?

A7: Unfortunately, this is not possible right now. The only way to do it would be to have a single repository for all the docs along with gitlab CI that would build different folders for different sites and redeploy the OpenShift applications. You will have separate site.docs.cern.ch , site-admin.docs.cern.ch, site-ops.docs.cern.ch etc. You can set access permissions according to the authorised readers.

Q8: I added _SSO proxy_ allowing only an e-group, which contains another e-group (I'm a member, of course). When I try to reach my .docs.cern.ch site, I get error _401 Unauthorized_. Why?

A8: The problem is on the e-groups side. Nested e-groups should indeed not pose a problem. What might be the issue is a delay in synchronising the e-group members (for newly created e-groups). Logging in via a private browser window might also provide an indication as to where the problem might be. If the problem is not solved overnight, please get in touch with ServiceNow and hint that the issue might be with e-groups / SSO. Please note that this process will change in the new .docs.cern.ch set-up.

Q9: I used incorrect Repository URL during the deployment or I simply want to change it, how can I do that? For example, I used the HTTPS URL of my git repository, including the deploy token I generated, but the build still fails with the following error:
  ```
  Cloning "https://gitlab+deploy-token-****:**********@gitlab.cern.ch/my-group/my-site " ...
  error: failed to fetch requested repository
  ```

A9: This process is valid in both cases. However if you experience the aforementioned error, check if the URL that you used ends with `.git`, and if not go to your OpenShift project, then in the menu on the left navigate to -> **Builds** -> **Builds**.
Here you will see all the builds in your project. Choose the build of your site (you can confirm it by checking if the Source information refers to your GitLab repository). This build should have the same name as the application's name. Click the build's name and it will display a history of your site builds. On the right you will see a button **Start Build** and a dropdown **Actions**. Click the **Actions** dropdown -> and then click **Edit**.
Here you will see the Source Configuration section with the `Git Repository URL` field. Add `.git` at the end of the URL, and click on **Save** button at the bottom of the page.

Once the correct URL is saved you will be redirected to the history page again. Click on the **Start Build** button which is placed next to the **Actions** button. This will build and deploy your documentation from the latest changes in the repository.
