# Automatic revision date

Users always appreciate to know when a given documentation page has been
last updated.

This is done with the [git-revision-date-localized
plugin](https://github.com/timvink/mkdocs-git-revision-date-localized-plugin),
which uses a nice localised human-friendly version of the date.

1. Add `mkdocs-git-revision-date-localized-plugin` to the
`requirements.txt` file as explained at the ["Custom Python packages"
page](custom_python_packages.md).

2. Activate the plugin by adding it to the list on your `mkdocs.yml` file:

```
plugins:
  - git-revision-date-localized
```

If you use the mkdocs-material theme, you should already see the last
revision on the bottom of all your pages.

[Link to the plug-in
documentation](https://timvink.github.io/mkdocs-git-revision-date-localized-plugin/index.html),
including examples for enabling the last revision date as a macro and
more.
