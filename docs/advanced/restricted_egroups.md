# Restrict the access to the members of specific egroups 

In order to change the `e-group(s)` whose members can access the site, you can customize the `default-role` role through the [Application portal](https://application-portal.web.cern.ch/){target=_blank}.

!!! warning

    Keep in mind, that in order for the followiing changes to take effect, you need to remember to create `GitlabPagesSite` with `anonymous` set to `false`. If you set it to `true` then your site will be publicly available without any restrictions. If you created it with an incorrect value, you can always change it by following the procedure described [here](../gitlabpagessite/modify_site_settings.md).

* First of all, find your application registration by using the top search bar. For this, check the [webeos project's name](https://webeos.cern.ch/){target=_blank} that holds your site ([the project that you created when you created or migrated your site](/gitlabpagessite/create_site/create_webeos_project/#create-a-new-project-on-webeoscernch){target=_blank}). If this project's name is, for example, `my-docs-site` your application registration name will be `webframeworks-webeos-my-docs-site`. The first part, `webframeworks-webeos-`, is always the same for all the GitLab Static Pages site's application registrations, so you can simply use the top search bar, type `webframeworks-webeos-`, select the `begins with` option in the `Application identifier:` and search for it, as you can see in the image.

![search_application_registration](/images/application-portal-search-application-registration.png)

* Once you have found it, click on the green button to edit it

![edit_application_registration](/images/application-portal-registration-edit.png)

* This will open the application details page. On the top bar click on the `Roles` tab. This will show you the list of roles defined for your application registration. Click the orange icon to `Edit role details`.

![Screenshot](/images/application-portal-roles.png)


* Uncheck the last checkbox <input type="checkbox"> `This role applies to all authenticated users` and click **Submit**.

![Screenshot](/images/application-portal-edit-role.png)


* One new green icon `Assign role to groups` will appear. Click on it.

![Screenshot](/images/application-portal-assign.png)


* Search and select the e-groups that you want to authorize to access your page. You can check the [groups portal](https://groups-portal.web.cern.ch/){target=_blank} for more information about egroups.

![Screenshot](/images/application-portal-add-egroup.png)


After these steps, the selected group is linked with the `default-role`. It means that only members of this egroup will have access to your documentation site.
