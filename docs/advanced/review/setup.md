# Temporarily review your documentation on OpenShift before deploying it: Setup

Follow these steps to set up your review workflow:

#### A. Generate a service account and token on OpenShift

##### A.1. Launch the OpenShift Client (CLI)

You need to have [docker](https://docs.docker.com/install/) installed and run in a terminal:

```bash
docker run -it gitlab-registry.cern.ch/paas-tools/openshift-client:latest /bin/bash
```

##### A.2. Login on OpenShift

Inside the docker container prompt run:

```bash
oc login https://openshift.cern.ch
```

and enter your CERN credentials.

##### A.3. Generate a service account and token

Inside the docker container prompt run:

```bash
oc create serviceaccount review -n [project_name]
oc policy add-role-to-user edit -z review -n [project_name]
oc serviceaccounts get-token review -n [project_name]
```

where `[project_name]` is essentialy the original site name you entered when you [created your site on Web Services](../../new/new_site.md) (for example `service-docs`). Make sure you note down the token.

#### B. Go to your git repository's CI / CD Settings on GitLab

Visit the URL of your git repository (for example `https://gitlab.cern.ch/service/docs`).

Hover over **Settings** on the left and click on **Repository**.

![Screenshot](/images/gitlab-settings_ci_cd-do.png)

---

#### C. Expand the **Variables** section

Click on **Expand** next to the **Variables** section.

![Screenshot](/images/gitlab-settings_ci_cd-variables-do.png)

---

#### D. Fill in the information

Add the following variables:

1. 
    * **Key**: Enter `OPENSHIFT_SERVER`.
    * **Value**: Enter `https://openshift.cern.ch`.
2. 
    * **Key**: Enter `OPENSHIFT_TOKEN`.
    * **Value**: Enter the token you generated in [A.3](setup.md#a3-generate-a-service-account-and-token).
3. 
    * **Key**: Enter `OPENSHIFT_PROJECT_NAME`.
    * **Value**: Enter the name of your OpenShift project. This is essentialy the original site name you entered when you [created your site on Web Services](../../new/new_site.md) (for example `service-docs`).
4. 
    * **Key**: Enter `OPENSHIFT_DOMAIN`.
    * **Value**: Enter `app.cern.ch`.
    
5. 
    * **Key**: Enter `MKDOCS_RELEASE`.
    * **Value**: Enter the version of Mkdocs that should be used to build the review environment. Possible values for the moment are 1.0 or 1.1

Use the default values for the rest of the fields:

* **Type**: Select `Variable`.
* **State (Protected)**: Leave unchecked <input type="checkbox">.
* **Masked (Masked)**: Leave unchecked <input type="checkbox">.
* **Scope**: Select `All environments`.

![Screenshot](/images/gitlab-settings_ci_cd-variables-filled_in.png)

---

#### E. Click on **Save variables**

![Screenshot](/images/gitlab-settings_ci_cd-variables-filled_in-do.png)

---

#### F. Add a CI / CD pipeline to your git repository on GitLab

Your git repository should normally look something like this:

```
mkdocs.yml
docs/
    index.md
    ...
```

Create a new file in the root folder of your git repository called `.gitlab-ci.yml` with the following contents:

```yaml
include:
  - project: 'authoring/documentation/s2i-mkdocs-review'
    ref: master
    file: 'gitlab-ci-template.yml'
```

Your git repository should now look something like this:

```
mkdocs.yml
docs/
    index.md
    ...
.gitlab-ci.yml
```

!!! note
    This predefined CI / CD pipeline is executed for all branches except for the `master` branch. When the `master` branch is updated [your documentation is automatically reployed anyway](../../new/redeploy.md). It is therefore important that you have correctly [entered the value of **Branch name or wildcard pattern to trigger on (leave blank for all)** as "master"](../../new/redeploy.md#h-fill-in-the-information).

You're all set. Go ahead and [learn how to use your review workflow](use.md).
