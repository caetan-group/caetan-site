# Temporarily review your documentation on OpenShift before deploying it: Use

Now that you've set up your review workflow it's time to use it:

#### A. Push your local development branch to your remote git repository

Locally clone your remote git repository:

```bash
git clone ssh://git@gitlab.cern.ch:7999/service/docs.git
```

Pull the latest master branch:

```bash
git checkout master
git pull
```

Create a local development branch and work on it:

```bash
git checkout -b [dev_branch_name]
...[some changes]...
```

Commit your work and push your local development branch to your remote git repository:

```bash
git add [some files]
git commit
git push origin [dev_branch_name]
```

This automatically triggers a new CI / CD pipeline on GitLab.

#### B. Examine your git repository's CI / CD Pipelines on GitLab

Visit the URL of your git repository (for example `https://gitlab.cern.ch/service/docs`).

Hover over **CI / CD** on the left and click on **Pipelines**.

![Screenshot](/images/gitlab-ci_cd-pipelines-do.png)

---

Your pipeline was just triggered and should be currently running.

![Screenshot](/images/gitlab-ci_cd-pipelines-review-running.png)

---

Upon successful completion it will be shown as **passed**.

![Screenshot](/images/gitlab-ci_cd-pipelines-review-passed.png)

---

This automatically creates a review environment on GitLab.

#### C. Examine your git repository's Environments on GitLab

Hover over **Operations** on the left and click on **Environments**.

![Screenshot](/images/gitlab-operations-environments-do.png)

---

Your recently created review environment is available here.

![Screenshot](/images/gitlab-operations-environments-available.png)

---

Click on **Open live environment** to access the temporary review deployment and review what your updates look like.

![Screenshot](/images/gitlab-operations-environments-available-open_live_environment.png)

!!! warning
    Note that temporary review deployments are by default publicly accessible in the CERN intranet. At the same time they are not publicly indexed anywhere and one needs to know what exact URL to access them at.

---

When ready, click on **Stop environment** to stop the temporary review deployment and perform the necessary clean-up.

![Screenshot](/images/gitlab-operations-environments-available-stop_environment.png)

!!! tip
    It is very important that you stop environments once they are no longer needed in order to keep your OpenShift project tidy and limit resource usage.

---

That's all there is to it. You may also be interested in [extending your review workflow](extend.md).
