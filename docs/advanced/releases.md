# Releases

Releases will be updated with the latest versions of their featured software, as long as there are no breaking changes.

## Release 1.3

* [MkDocs 1.2.1](https://www.mkdocs.org/about/release-notes/#version-121-2021-06-09) ([PyPI](https://pypi.org/project/mkdocs/1.2.1/))
* [Material for MkDocs 7.1.7](https://squidfunk.github.io/mkdocs-material/changelog/#717-_-june-6-2021) ([PyPI](https://pypi.org/project/mkdocs-material/7.1.7/))

The upgrade process is the same as in case of upgrading from 1.1 to 1.2.

### New relevant feature

[Support for internationalization (i18n)](https://squidfunk.github.io/mkdocs-material/setup/changing-the-language/) using the plugin [mkdocs-static-i18n](https://github.com/ultrabug/mkdocs-static-i18n). To see how to deploy your documentation in multiple languages you can follow this [guide](multi_language.md).


You can also check an example of how to configure your site [here](https://gitlab.cern.ch/authoring/documentation/s2i-mkdocs-example). Notice that for a multilanguage site, files need to follow the name format `<file_name>.<language>.md`, so **if you want to deploy a multilanguage site please, don't use an `index.md` file** as it is used in the example since this file is for single language site configured with the old configuration settings. If you still want to use the old configuration settings for a single language site please, check [the single language site section](single_language.md).

This new feature will not affect at all your current mono-language documentation site, so if you want to keep your site as it currently is with just one language you do not have to do anything.

## Release 1.2

* [MkDocs 1.1.2](https://www.mkdocs.org/about/release-notes/#version-112-2020-05-14) ([PyPI](https://pypi.org/project/mkdocs/1.1.2/))
* [Material for MkDocs 6.2.5](https://squidfunk.github.io/mkdocs-material/changelog/#625-_-january-17-2021) ([PyPI](https://pypi.org/project/mkdocs-material/6.2.5/))

The upgrade process is the same as in case of upgrading from 1.0 to 1.1.

## Release 1.1

* [MkDocs 1.1](https://www.mkdocs.org/about/release-notes/#version-11-2020-02-22) ([PyPI](https://pypi.org/project/mkdocs/1.1/))
* [Material for MkDocs 5.1.0](https://squidfunk.github.io/mkdocs-material/releases/changelog/#510-_-april-12-2020) ([PyPI](https://pypi.org/project/mkdocs-material/5.1.0/))
* [Nginx 1.16.1](https://nginx.org/en/CHANGES-1.16) ([S2I](https://github.com/sclorg/nginx-container/tree/master/1.16))
* [Python 3.6.9](https://www.python.org/downloads/release/python-369/) ([Software Collections](https://www.softwarecollections.org/en/scls/rhscl/rh-python36/))

### How to upgrade from Release 1.0

There are 2 stages in upgrading from release 1.0 to 1.1:

1. First, update your MkDocs configuration (`mkdocs.yml`) in case you are affected by the changes in the new version of Material for MkDocs (step A);
2. Then, instruct OpenShift to use the new release (steps B-F).

#### A. Update your MkDocs configuration (`mkdocs.yml`)

Material for MkDocs has been updated to version 5.x in release 1.1 and that might bring some changes to your MkDocs configuration (`mkdocs.yml`). Please carefully read the [upstream guide on how to upgrade to 5.x](https://squidfunk.github.io/mkdocs-material/releases/5/#how-to-upgrade) and apply the necessary changes to your files in your git repository if applicable.

If you've followed the rest of the how-to guide (and specifically the [step on automatically redeploying your documentation](../new/redeploy.md)), you know that updating your git repository means that your documentation will automatically be redeployed. Depending on your specific configuration this might temporarily lead to a failed build on OpenShift (harmless and unnoticeable) or a slightly unexpected behaviour of your documentation (most likely harmless and unnoticeable), until your instruct OpenShift to use the new release. Follow the next steps (B-F) as soon as possible to limit this temporary inconvenience.

#### B. Go to your OpenShift project

![Screenshot](/images/openshift-project-deployed-customised_URL.png)

---

#### C. Go to your OpenShift project Builds

Hover over **Builds** on the left and click on **Builds**.

![Screenshot](/images/openshift-project-builds-do.png)

---

#### D. Go to your documentation build

Click on **docs** from the list of builds. Unless you have a more complicated set-up you should only have one build in the list. The name **docs** might be different for you and it's essentially the **application name** you provided when you first [selected MkDocs and deployed your documentation](../new/deploy.md#e-fill-in-the-information).

![Screenshot](/images/openshift-project-builds-build-do.png)

---

![Screenshot](/images/openshift-project-builds-build.png)

---

#### E. Edit your documentation build

Click on the **Actions** menu at the upper right corner and click on **Edit**.

![Screenshot](/images/openshift-project-builds-build-edit-do.png)

---

![Screenshot](/images/openshift-project-builds-build-edit.png)

---

#### F. Update the release

Under the **Image Configuration**, **Build From** change the **openshift / mkdocs tag** from **1.0** to **1.1** and click on **Save** at the bottom of the page.

![Screenshot](/images/openshift-project-builds-build-edit-tag_selected.png)

---

![Screenshot](/images/openshift-project-builds-build-edit-save-do.png)

---

## Release 1.0

* [MkDocs 1.1](https://www.mkdocs.org/about/release-notes/#version-11-2020-02-22) ([PyPI](https://pypi.org/project/mkdocs/1.1/))
* [Material for MkDocs 4.6.3](https://squidfunk.github.io/mkdocs-material/releases/changelog/#463-_-february-14-2020) ([PyPI](https://pypi.org/project/mkdocs-material/4.6.3/))
* [Nginx 1.16.1](https://nginx.org/en/CHANGES-1.16) ([S2I](https://github.com/sclorg/nginx-container/tree/master/1.16))
* [Python 3.6.9](https://www.python.org/downloads/release/python-369/) ([Software Collections](https://www.softwarecollections.org/en/scls/rhscl/rh-python36/))
