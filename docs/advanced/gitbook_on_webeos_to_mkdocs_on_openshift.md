# Keep all your documentation links the same if your are migrating from GitBook on WebEOS to MkDocs on OpenShift

If your existing documentation is based on GitBook and hosted on WebEOS, the URLs will look something like this: `https://service-docs.web.cern.ch/service-docs/chapter/section.html`. When you migrate to MkDocs on OpenShift following this how-to guide, the URLs will look something like this by default: `https://service-docs.web.cern.ch/chapter/section/`. Therefore, after migrating from GitBook on WebEOS to MkDocs on OpenShift, all URLs to your documentation will be *broken* (except of course for the base URL: `https://service-docs.web.cern.ch`). Provided that you've kept your existing hostname when [migrating your site](../existing/migrate_site.md) and [customising your documentation URL](../existing/route.md), there is an easy way to maintain all the URLs after migrating from GitBook on WebEOS to MkDocs on OpenShift.

#### A. Customise the MkDocs configuration: `mkdocs.yml`

By default, MkDocs creates URLs as *directories*. For example the `chapter.md` file will result in the `/chapter/` URL. The [`use_directory_urls` configuration option](https://www.mkdocs.org/user-guide/configuration/#use_directory_urls) (when set to `false`) allows to change this behaviour and create URLs as *files*. For example the `chapter.md` file would result in the `/chapter.html` URL. To enable this feature the following line needs to be added to the MkDocs configuration file `mkdocs.yml`:

``` yaml
use_directory_urls: false
```

A more complete MkDocs configuration file `mkdocs.yml` would look something like this:

``` yaml
site_name: Service Documentation
site_description: Documentation for Service
site_author: Service Documentation Authors
site_url: https://service-docs.web.cern.ch/

repo_name: GitLab
repo_url: https://gitlab.cern.ch/service/docs
edit_uri: 'blob/master/docs'

theme:
    name: material

nav:
    - Introduction: index.md

use_directory_urls: false
```

More information can be found at the [respective MkDocs user guide article](https://www.mkdocs.org/user-guide/configuration/#use_directory_urls).

So far you've managed to get the URLs to look something like this with MkDocs on OpenShift: `https://service-docs.web.cern.ch/chapter/section.html`. Now you also have to do something about the repetition of the `service-docs` site name in the URL.

#### B. Customise the Nginx server configuration that is serving your documentation on OpenShift

As described in more detail in the [respective advanced topic](nginx.md) you can provide custom configuration for the Nginx server that is serving your documentation on OpenShift. You can profit from this feature to permanently redirect URLs appropriately. This way you will avoid having *broken* URLs and users will be bookmarking the correct URLs from now on.

In the context of the migration from GitBook on WebEOS to MkDocs on OpenShift, you want to redirect URLs when the repetition of the `service-docs` site name is in the URL. In the root directory of your git repository create a directory called `nginx-default-cfg`. In that directory create a file called `redirect.conf` (it can have a different name of your choise, as long as it ends in `.conf`).

Your git repository should look something like this:

```
mkdocs.yml
docs/
    index.md
nginx-default-cfg/
    redirect.conf
```

The contents of the `redirect.conf` file should be:

```
location = /service-docs {
    return 301 /;
}
location /service-docs/ {
    rewrite /service-docs/(.*) /$1 permanent;
}
```

Make sure you replace all 3 instances `service-docs` in `redirect.conf` with your actual site name.

You've now made sure that all the URLs of your existing documenation site with GitBook on WebEOS are maintained with MkDocs on OpenShift: `https://service-docs.web.cern.ch/chapter/section.html` will work *out of the box* and `https://service-docs.web.cern.ch/service-docs/chapter/section.html` will be automaticaly redirected to `https://service-docs.web.cern.ch/chapter/section.html`.
