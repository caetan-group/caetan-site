# Introduction

GitLab Static Pages is the new infrastructure for documentation site hosting which makes the creation of static sites easier. You can follow this documentation to migrate your site from the old infrastructure or to create a new site using GitLab Pages. It is now possible to create documentation sites using different SSG (static site generators) e.g. Hugo, Jekyll, MkDocs etc.

1. [Create a new GitLab Static Pages site](create_site/index.md).
2. [Migration from the old MkDocs sites](migration/index.md).
3. [Modify your site's settings](modify_site_settings.md).
4. [Review before deploying](review/index.md).


## Contact

For any question or issue please, contact us by opening a [SNOW Request](https://cern.service-now.com/service-portal/?id=sc_cat_item&name=request&se=Authoring-Service).
