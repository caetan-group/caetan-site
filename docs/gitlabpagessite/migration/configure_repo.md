# Configure your GitLab repository

If you want an easy and automatic way to have the changes automatically deployed, we recommend you to [include a special template](https://gitlab.cern.ch/authoring/documentation/mkdocs-ci/-/blob/master/mkdocs-gitlab-pages.gitlab-ci.yml){target=_blank} on top of your `.gitlab-ci.yml` by copying there the following piece of code:
    ```
    include:
        - project: 'authoring/documentation/mkdocs-ci'
          file: 'mkdocs-gitlab-pages.gitlab-ci.yml'
    ```
    
It is not necessary to modify anything on it unless you do not want to use the `master` branch (the default one) for your documentation files.

If everything is configured properly, each new commit on the master branch should trigger a new pipeline.

![pipelines](/images/gitlab-pipelines-migration.png)

!!! info
    For more context: in order to have the changes automatically deployed we need to build the MkDocs documentation artifacts and push them (pushing is fully handled by GitLab so you don't need to worry about it). Thanks to the previous `.gitlab-ci.yml` configuration which you should use, static content will be built and pushed every time a new commit is pushed to the `master` branch (or if a branch is merged to `master` branch). With this new setup, all your future changes will be automatically deployed once the GitLab CI job rebuilds your static site with your latest changes.

!!! warning
    A specific job called `pages` in the configuration file makes GitLab aware that you are deploying a GitLab Pages website. This job is added to your repository thanks to the previous step where you included a template to your `.gitlab-ci.yml`, so you do not have to do anything. This `pages` job is branch agnostic, which means that it is supposed to be run only in the event of changes added to a specific branch (`master` by default). If it runs for a different branch than the previous one that you were using, the documentation hosted on one of your custom domains will be updated as well and that might lead to the changes being visible to everyone accessing your site. In other words, if you have a testing branch and you run it, it will completely overwrite the previous content with the one of the testing branch.


## Next step 

Good! your repository is configured. Now you can go to the next step to [create a webEOS project](/gitlabpagessite/migration/create_webeos_project/).
