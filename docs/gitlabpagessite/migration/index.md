# Migration from the old infrastructure to the new GitLab Pages based static sites

This section will help you migrate your MkDocs site to the new infrastructure.

!!! warning
    If your documentation site has been hosted under `.web.cern.ch` (the following note doesn't apply to sites hosted under `.docs.cern.ch`) and you would like to keep the current URL, you will need to first `free` the URL by deleting the current site, via the [WebServices Portal](https://webservices.web.cern.ch). It may take up to 1h until the URL can be reused by your migrated site, during that time the site will not be accessible. Please take this potential downtime into account when planning migrations of your more critical, user-facing sites.

1. [Configure your GitLab repository](/gitlabpagessite/migration/configure_repo/).
2. [Create a webEOS project](/gitlabpagessite/migration/create_webeos_project/).
3. [Set up Pages](/gitlabpagessite/migration/set_up_pages/).
4. [Access restriction](/gitlabpagessite/migration/access_restriction/).
4. [Keep it clean](/gitlabpagessite/migration/keep_it_clean/).
