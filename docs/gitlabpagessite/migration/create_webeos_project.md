# Create a new project on [webeos.cern.ch](https://webeos.cern.ch/){target=_blank}

Once your GitLab repository is fully configured, you have to create a webEOS project and to do this, go to [webEOS](https://webeos.cern.ch/){target=_blank} and create a new project, checking on the left side menu that the **Administrator** option is selected (and **not** Developer).

This will show a form with the following fields:

* **Name:** This is the name of your project. This field is mandatory. It is good practice to use the same name as your GitLab's repository name, and ending with `-docs` (i.e. `migration-example-docs`).
* **Display Name**: Enter the display name of the project (i.e. `My Service documentation`).
* **Description**: Enter a description of your site. This field is mandatory.

![project](/images/openshift-project-creation-migration.png)

## Create a site

Once your project is created, on the left bar do **one** of the following (depending on if you still are in the `Administrator` view or you moved to `Developer` view):

* **A.** If you are still in the `Administrator` view, go to the **Operators** -> **Installed Operators**. This will show the list of all available operators. Make sure that you are in the correct `Project`.
![installed_operators](/images/openshift-project-installed-operators-admin.png)

* **B.** If you have moved from views and now you are in the `Developer` view, navigate to **+Add** -> **Operator backed**. This will show the list of all available operators. Make sure that you are in the correct `Project`.
![installed_operators](/images/openshift-project-installed-operators-developer.png)

Search for `GitLab Pages Site Operator` and click on it. This will open the operator description, similar to the following image:

![operator](/images/openshift-project-create-site-operator.png)

To create your new site click on `Create instance` on the bottom of the box or go to the **Publish a static site from a GitLab repository (gitlab.cern.ch)** tab in the top bar as you can see in the previous image. This will show the list of your sites created in this project and a `Create GitlabPagesSites` button in the top-right part. Click on this button and it will open a form with the following fields:

!!! warning
    If your MkDocs site has been using `web.cern.ch` domain, then make sure that you delete the site from the [WebServices Portal](https://webservices.web.cern.ch) if you have not done it yet. This is required to release the domain so you can use it in the next steps. Please wait for an email confirmation that the site has been deleted before proceeding with the migration.

* **Name:** This is the name of your site. Since this name has to be unique we recommend you to use the same name as you have used as your GitLab's repository name (i.e. `migration-example-docs`), but since this is just a name for your project this will not have further implications.
* **Labels (optional):** Optional labels to give more information about your site. You can leave it empty.
* **Host:** This is the current URL of your site, for example `migration-example.docs.cern.ch`. **Notice** that you do not have to put the `http/s` protocol at the beginning.
* **Anonymous:** It is a true/false toggle that will make your site accessible without authentication. If false it will redirect users to the CERN Single Sign-On in order to authenticate them before accessing your site.

![operator form](/images/openshift-project-create-site-operator-form-migration.png)

!!! warning
    Remember that the `Host` has to be the same as one of the defined GitLab Pages custom domains, hence the same restrictions apply in order to create your hostname's site:

    * Domain (`migration-example` in the previous image) only allows the letters, numbers and hyphen symbol ( - ).
    * No hostname can live under a domain that is not supported (`docs.cern.ch` and `web.cern.ch` are allowed; we strongly suggest to use `docs.cern.ch` for new sites and `web.cern.ch` for sites that used the domain in the old infra)
    * No hostname can have its first segment being shorter than 4 characters


Once you are done, click on **Create** and your new site will be shown in the list of your project's sites.


## Next step 

Perfect! Your webEOS project is already configured and it has already a URL (the **Host**) from where people will be able to access your site. Remember this URL, because it is important for [the next step for setting up GitLab Pages](/gitlabpagessite/migration/set_up_pages/).
