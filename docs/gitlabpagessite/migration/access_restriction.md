# Access restriction

If your site was accessible only to members of specific groups, then follow the instructions described [here](../../advanced/restricted_egroups.md) to restrict the access to your new site.
