# Keep it clean

Some things are not necessary anymore therefore you should remove them (e.g. deploy token, webhook definition).

### Remove webhook

Go to your git repository on [GitLab](https://gitlab.cern.ch/){target=_blank} and hover over **Settings** on the left panel. Click on **Webhooks** option

![Screenshot](/images/gitlab-settings_integrations-do-1.png)

and click on **Delete** button to delete the webhook that were being used to trigger Openshift builds. You can find it at the bottom of the page as you can see in the following image:

![webhook](/images/gitlab-settings-remove-webhook.png)


### Remove deploy token if it had been created before

Go to your git repository on [GitLab](https://gitlab.cern.ch/){target=_blank} and hover over **Settings** on the left panel. Click on **Repository** option.

![Screenshot](/images/gitlab-settings_repository-do.png)

Now, expand the **Deploy Tokens** section 

![Screenshot](/images/gitlab-settings_repository-deploy_tokens-do.png)

and remove (revoke) the token that were being used for the Openshift builds by clicking on the **Revoke** button as you can see in the following image:

![revoke](/images/gitlab-settings-revoke-token.png)


### Remove your old OpenShift 3 project

Go to the [Web Services portal](https://webservices.web.cern.ch) and click on the **My websites** tab. Here you will see all your websites, just click on the website that you want to remove and click on `Remove name-of-your-website` in the `Toolbox for current site`.


### Restore `https://cern.ch/site-name` redirection

If your site was accessible via `https://cern.ch/<site-name>` then you will need to restore that by creating a redirection to a URL. Go to the [Web Services Portal](https://webservices.web.cern.ch/webservices/Services/RegisterExternalUrl/Default) and fill in the form to create a redirection, e.g. if I would like to support a redirection from `https://cern.ch/how-to` to `https://how-to.web.cern.ch` I would need to fill in the form with the following values:

- Site name: `how-to`
- Description: HowTo documentation
- Redirection URL: `http://how-to.web.cern.ch` (only `http://` is accepted but the redirection will use https if the initial request uses `https://`)


### Remove UserProvidedDirectory
!!! warning
    This step is just required for those who used a `WebEOS` site with a custom `.htaccess` file to redirect one URL to another.

Please, remove the corresponding `UserProvidedDirectory` from the `WebEOS Site Management Operator` in [webeos](https://webeos.cern.ch).

![upd](/images/openshift-project-upd.png)

![delete upd](/images/openshift-project-delete-redirection-upd.png)


In case you still want a redirection to `.web.cern.ch`, please, create another `GitlabPagesSite` with a `.web.cern.ch` host.

![redirect](/images/openshift-project-create-redirection.png)

Optionally, you can remove this old `.htaccess` file, which is not necessary anymore. This file is present in your `www` folder on [CERNBox](https://cernbox.cern.ch):

- `/eos/user/<initial>/<userID>/www` if it was a personal website
- `/eos/project/<initial>/<project name>/www` if it was a project website

More information about CERNBox can be found by [clicking here](https://cernbox-manual.web.cern.ch/cernbox-manual/en/web/).
