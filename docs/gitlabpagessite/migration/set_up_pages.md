# Set up Pages

Great! You are almost done. Now that you have also created your [webeos](https://webeos.cern.ch/){target=_blank} project you can configure the GitLab Pages for your repository.

## Enable Pages in a repository

Now, it is time to go back to the [GitLab repository](https://gitlab.cern.ch/) that contains your documentation files will have a new `Settings` option called `Pages`

![settings](/images/gitlab-settings-pages.png)

Notice that to be able to see `Settings` you have to have, at least, a Maintainer role.

## Add a custom Pages domain

First of all, make sure that `Force HTTPS (requires valid certificates)` is ticked off and then click on the `Save changes` blue button. Your site will be accessible via HTTPS in any case thanks to the new infrastructure.

![certificates](/images/gitlab-https-pages.png)

In order to create your custom site's URL you have to click on the upper right button **New Domain**. Now, to create your domain please, use the current URL that is used by the old site (which is the same as the **Host** value that you used in the previous step when [creating a webeos project](/gitlabpagessite/migration/create_webeos_project/#create-a-site){target=_blank}).

![domain](/images/gitlab-domain-pages.png)

!!! warning
    There are some restrictions in order to create your domain:

    * Domain (`migration-example` in the previous image) only allows the letters, numbers and hyphen symbol ( - ).
    * No hostname can live under a domain that is not supported (`docs.cern.ch` and `web.cern.ch` are allowed; we strongly suggest to use `docs.cern.ch` for new sites and `web.cern.ch` for sites that used the domain in the old infra)
    * No hostname can have its first segment being shorter than 4 characters


You can leave the **Certificate (PEM)** and **Key (PEM)** fields empty if you have previously disabled the button `Force HTTPS (requires valid certificates)`.

Click on **Create New Domain** button and then on the button **Save Changes**. If everything goes well you will see the new domain in the `Domains` list and your new site should be available after a few minutes.


## Done!

Congratulations, your site is ready! You can access it via the URL that you set as the domain. Now it is time [to clean up everything a little bit](/gitlabpagessite/migration/keep_it_clean/). You can also optionally [restrict the access to your site](/gitlabpagessite/migration/access_restriction/).
