# Modify your site's settings

To modify the configuration of your site you will have to manually edit the YAML file. In order to do this please, follow these guidelines:

In the left menu, go to the **Operators** section and click on **Installed Operators**. This will show the list of the available operators that are installed. Make sure that you are in the correct `Project`.

![installed_operators](/images/openshift-project-installed-operators.png)


Search for `GitLab Pages Site Operator`, click on it and go to the **Publish a static site from a GitLab repository (gitlab.cern.ch)** tab in the top bar. This will open a list of your sites available in the current project. Once you have located the site that you want to modify click on the three dots and select the `Edit GitlabPagesSite`.

![edit_operator](/images/openshift-project-edit-site-operator.png)


This will open a YAML editor where you can edit your site's configuration. At the bottom of the file, you can see the `spec:` section.

- `anonymous`: change to true/false as you wish.
- `host`: change to the new host (it must be one of the GitLab Pages custom domain)

!!! tip
    You can check [here](/gitlabpagessite/create_site/create_webeos_project/#create-a-site){target=_blank} what these fields are used for.

Once you are happy with your changes click on the **Save** button.
