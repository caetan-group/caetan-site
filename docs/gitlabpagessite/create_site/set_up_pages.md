# Set up GitLab Pages

Great! You are almost done. Now that you have also created your [webeos](https://webeos.cern.ch/){target=_blank} project you can configure the GitLab Pages of your documentation repository to serve it.

## Enable Pages in a repository

Now, it is time to go back to the [GitLab repository](https://gitlab.cern.ch/) that contains your documentation files will have a new `Settings` option called `Pages`

![settings](/images/gitlab-settings-pages.png)

Notice that to be able to see `Settings` you have to have, at least, a Maintainer role.

## Add a custom Pages domain

First of all, make sure that `Force HTTPS (requires valid certificates)` is ticked off and then click on the `Save changes` blue button. Your site will be accessible via HTTPS in any case thanks to the new infrastructure.

![certificates](/images/gitlab-https-pages.png)

In order to create your custom site's URL you have to click on the upper right button **New Domain**.
Now, to create your domain please, use the same URL that you used before for the **Host** when you [created your site](/gitlabpagessite/create_site/create_webeos_project/#create-a-site).

![domain](/images/gitlab-domain-new-site.png)

!!! warning
    There are some restrictions in order to create your domain:

    * Domain (`my-new-gitlab-pages-site` in the previous image) only allows the letters, numbers and hyphen symbol ( - )
    * No hostname can live under a domain that is not supported (`docs.cern.ch` and `web.cern.ch` are allowed; we strongly suggest to use `docs.cern.ch` for new sites and `web.cern.ch` for sites that used the domain in the old infra)
    * No hostname can have its first segment being shorter than 4 characters


You can leave the **Certificate (PEM)** and **Key (PEM)** fields empty if you have previously disabled the button `Force HTTPS (requires valid certificates)`.

Click on **Create New Domain** button and then on the button **Save Changes**. If everything goes well you will see the new domain in the `Domains` list. Your new site should be available in a few minutes.


## Access restriction

If your site is protected with CERN SSO and you want to restrict the access to your site only to members of specific groups, then you can follow the instructions described [here](../../advanced/restricted_egroups.md).


## Done!

Congratulations, your site is ready! You can access it through the URL that you set as domain. If you want some tips about how to create files in GitLab you can [check this section](/gitlabpagessite/create_site/create_new_file/).
