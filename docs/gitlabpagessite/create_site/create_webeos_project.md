# Create a new project on [webeos.cern.ch](https://webeos.cern.ch/){target=_blank}

Once your GitLab repository is fully configured, go to [webEOS](https://webeos.cern.ch/){target=_blank} and create a new project, check on the left side menu that the **Administrator** option is selected (and **not** Developer).

This will show a form with the following fields:

* **Name:** This is the name of your project. This field is mandatory. It is a good practice to use the same name as your GitLab's repository name, and end it with `-docs` (i.e. `my-service-name-docs`).
* **Display Name**: Enter the display name of the project (i.e. `My Service documentation`).
* **Description**: Enter a description of your site. This field is mandatory.

![project](/images/openshift-project-creation.png)


## Create a site

Once your project is created, on the left bar do **one** of the following (depending on if you are still in the `Administrator` view or you moved to `Developer` view):

* **A.** If you are still in the `Administrator` view, go to the **Operators** -> **Installed Operators**. This will show the list of all available operators. Make sure that you are in the correct `Project`.
![installed_operators](/images/openshift-project-installed-operators-admin.png)

* **B.** If you have moved from views and now you are in the `Developer` view, navigate to **+Add** -> **Operator backed**. This will show the list of all available operators. Make sure that you are in the correct `Project`.
![installed_operators](/images/openshift-project-installed-operators-developer.png)

Search for `GitLab Pages Site Operator` and click on it. This will open the operator description, similar to the following image:

![operator](/images/openshift-project-create-site-operator.png)

To create your new site click on `Create instance` on the bottom of the box or go to the **Publish a static site from a GitLab repository (gitlab.cern.ch)** tab in the top bar. This will show the list of your sites created in this project (if any) and a `Create GitlabPagesSites` blue button in the top-right part. Click on this button and it will open a form with the following fields:

* **Name:** This is the name of your site. Since this name has to be unique we recommend you to use the same name as you have used as a project name (i.e. `my-new-gitlab-pages-site-docs`), but since this is just a name for your project this will not have further implications.
* **Labels (optional):** Optional labels to give more information about your site. You can leave it empty.
* **Host:** This is the URL of your site, for example `my-new-gitlab-pages-site.docs.cern.ch`. **Notice** that you do not have to put the `http/s` protocol at the beginning.
* **Anonymous:** It is a true/false toggle that will make your site accessible without authentication. If false it will redirect users to the CERN Single Sign-On in order to authenticate them before accessing your site.

![operator form](/images/openshift-project-create-site-operator-form.png)

!!! warning
    * Domain (`my-new-gitlab-pages-site` in the previous example) only allows the letters, numbers and hyphen symbol ( - ).
    * No hostname can live under a domain that is not supported (`docs.cern.ch` and `web.cern.ch` are allowed; we strongly suggest to use `docs.cern.ch` for new sites and `web.cern.ch` for sites that used the domain in the old infra)
    * No hostname can have its first segment being shorter than 4 characters


Once you are done, click on **Create** and your new site will be shown in the list of your project's sites.


## Next step 

Perfect! Your webEOS project is already configured and it has already a URL (the **Host**) from where people will be able to access your site. Remember this URL, because it is important for [the next step for setting up GitLab Pages](/gitlabpagessite/create_site/set_up_pages/).
