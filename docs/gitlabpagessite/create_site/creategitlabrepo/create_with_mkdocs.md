# Option B. Create a site using MkDocs (Markdown)

If you prefer to use the MkDocs with Markdown sources for your documentation rather than a GitLab template, you should follow this [structure](https://www.mkdocs.org/user-guide/writing-your-docs/){target=_blank}.

Depending on your experience and/or how brave you are, you can choose **one** of the following options to start developing your documentation:

**A.** [Import project](create_with_mkdocs.md#a-import-project): If you want to start with an example MkDocs structure, continue by importing an example project.

**B.** [Blank project](create_with_mkdocs.md#b-blank-project): If you are confident with structuring your git repository on your own, continue with a blank project.


## A. Import project

Create a new project by clicking on the upper right button **New project** and select the **Import project** option. This will show several importing sources. Choose the **Repo by URL** option. It will open a form to fill in the information:

* **Git repository URL**: Enter: `https://gitlab.cern.ch/authoring/documentation/mkdocs-container-example.git`. This will help you start with a very simple example MkDocs structure with minimal content.
* **Username (optional)**: Leave empty.
* **Password (optional)**: Leave empty.
* **Mirror repository**: Leave unchecked.
* **Project name**: Enter a descriptive name for this git repository. For example "My new GitLab Pages site". This will help you and other users remember what this git repository is for.
* **Project URL**: Select one of the available options, essentially the *Groups* and *Users* you have access to on GitLab. There might already exist a group for your service, otherwise, we suggest you [create one](https://gitlab.cern.ch/groups/new){target=_blank} first and then select it here. This, together with the **Project slug** will form the URL of your git repository.
* **Project slug**: Enter the slug of your git repository. This, together with the **Project URL**, will form the URL of your git repository. For example, if you selected `service` as the **Project URL** and `docs` as the **Project slug**, your git repository will be available at `https://gitlab.cern.ch/service/docs`. Please, we encourage you to use `docs` as the `Project slug` for static documentation sites, as you can see in the following example.
* **Project description (optional)**: Enter a description of your git repository. For example "This is my new GitLab Pages site". This will help you and other users remember what this git repository is for.
* **Visibility Level**: Select the visibility level of your git repository. This only affects who has access to the Markdown sources for your documentation.
    * "Private" means you have to explicitly give read access to other users.
    * "Internal" means all CERN users have read access.
    * "Public" means that everyone on the Internet has read access.

!!! warning
    The name and the URL of your git repository are completely independent of your site name and URL.

![form](/images/gitlab-import-repo.png)


Once you filled in the form, click on **Create project** and a repository will be created with the sample of a MarkDown site.


## B. Blank project

Create a new project by clicking on the upper right button **New project** and select the **Create blank project** option. This will open a form to fill in the information:

* **Project name**: Enter a descriptive name for this git repository. For example "My new GitLab Pages site". This will help you and other users remember what this git repository is for.
* **Project URL**: Select one of the available options, essentially the *Groups* and *Users* you have access to on GitLab. There might already exist a group for your service, otherwise, we suggest you [create one](https://gitlab.cern.ch/groups/new){target=_blank} first and then select it here. This, together with the **Project slug** will form the URL of your git repository.
* **Project slug**: Enter the slug of your git repository. This, together with the **Project URL**, will form the URL of your git repository. For example, if you selected `service` as the **Project URL** and `docs` as the **Project slug**, your git repository will be available at `https://gitlab.cern.ch/service/docs`. Please, we encourage you to use `docs` as the `Project slug` for static documentation sites, as you can see in the previous example.
* **Project description (optional)**: Enter a description of your git repository. For example "This is my new GitLab Pages site". This will help you and other users remember what this git repository is for.
* **Visibility Level**: Select the visibility level of your git repository. This only affects who has access to the Markdown sources for your documentation.
    * "Private" means you have to explicitly give read access to other users.
    * "Internal" means all CERN users have read access.
    * "Public" means that everyone on the Internet has read access.

![form](/images/gitlab-create-blank-repo.png)


Once you have filled the form, click on **Create project** and an empty repository will be created.


### Build artifacts

**Only if you opt for creating a blank project**, the last step is to build and push the artifacts needed to deploy the site. If you want an easy and automatic way to do it we recommend you to [include a special template](https://gitlab.cern.ch/authoring/documentation/mkdocs-ci/-/blob/master/mkdocs-gitlab-pages.gitlab-ci.yml){target=_blank} on top of your `.gitlab-ci.yml` by copying there the following piece of code:
    ```
    include:
        - project: 'authoring/documentation/mkdocs-ci'
          file: 'mkdocs-gitlab-pages.gitlab-ci.yml'
    ```

This will build and push to a safe storage the static assets every time a new commit is pushed to the `master` branch (or a branch is merged to `master` branch). You can modify it to your liking.

!!! info
    For more context: in order to have the changes automatically deployed we need to build the MkDocs documentation artifacts and push them (pushing is fully handled by GitLab so you don't need to worry about it). Thanks to the previous `.gitlab-ci.yml` configuration which you should use, static content will be built and pushed every time a new commit is pushed to the `master` branch (or if a branch is merged to `master` branch). With this new setup, all your future changes will be automatically deployed once the GitLab CI job rebuilds your static site with your latest changes.

!!! note
    In case you opted for the [Import project](create_with_mkdocs.md#a-import-project) option your `.gitlab-ci.yml` will already include this template, so you will not have to perform this step.

If everything goes as expected you will see a new job in the CI/CD -> Pipelines page:

![pipelines](/images/gitlab-pipelines-migration.png)

---

!!! warning
    With this new setup all changes are automatically deployed once the GitLab CI job rebuilds your static site.


## Next step 

Good! your repository is configured. Now you can go to the next step to [create a webEOS project](/gitlabpagessite/create_site/create_webeos_project/).
