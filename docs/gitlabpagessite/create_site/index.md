# Create a new MkDocs-based documentation site

With the steps listed below, you will be able to deploy your documentation site from your GitLab Pages repository.

1. [Create a new GitLab repository](/gitlabpagessite/create_site/create_gitlab_repo/).
2. [Create a webEOS project](/gitlabpagessite/create_site/create_webeos_project/).
3. [Set up Pages](/gitlabpagessite/create_site/set_up_pages/).
4. [Create files](/gitlabpagessite/create_site/create_new_file/).
