# How to delete a site

This section explains how you can remove your site. Since there are several stages that you may want to perform (from just removing the URL to removing all your resources), here you will see several subsections, so you can follow one or more depending on your needs.

1. [Delete Gitlab Pages content/config](/gitlabpagessite/delete_site/gitlab/).
2. [Delete webEOS resources](/gitlabpagessite/delete_site/webeos/).


!!! note
    In order to remove the site completely you need to perform all the steps.
