# On [GitLab](https://gitlab.cern.ch){target=_blank}

## Remove your site's domain

In case that you want to unlink a specific Pages domain from your GitLab repository, navigate to the page of your repository and, in the left panel go to the `Settings` option called `Pages`.

![settings](/images/gitlab-settings-pages.png)


Click on the red `Remove` button that is right next to the pages domain(s) you want to remove.

![remove_domain](/images/gitlab-remove-domain.png)


You can create this domain again later on and, if you did not change/remove anything else, you will end up in the same scenario as before, without the need of running the pipeline again since all the configuration and resources were not removed. This could be useful if you want to quickly hide your documentation site.

!!! warning
    If you remove the domain and then you create a different one remember that you will need to change also the `host` in your [webEOS](https://webeos.cern.ch){target=_blank} project. **NOTICE** that the new one may be already taken, so you might not be able to use it.


It is also possible to remove all custom domains at once by clicking the `Remove pages` button on the bottom of the page.

![remove_pages](/images/gitlab-remove-pages.png)


If you chose this option, notice that this will also remove your site resources. It means that in case you want to recreate your previous domain(s) you will need to run the pipeline again. For this, go to CI/CD section in the left panel of your repository and click on the Pipelines option. Once there, click on the upper right button **Run Pipeline** and wait until the job ends successfully.

![pipelines](/images/gitlab-pipelines-migration.png)
