# On [webEOS](https://webeos.cern.ch/){target=_blank}

In order to remove your GitLab Pages Site from [webEOS](https://webeos.cern.ch/){target=_blank} select the project that holds your site. Once you are in the project, on the left bar do **one** of the following (depending on if you are in the `Administrator` view or you are in `Developer` view):

* **A.** If you are in the `Administrator` view, go to the **Operators** -> **Installed Operators**. This will show the list of all available operators. Make sure that you are in the correct `Project`.
![installed_operators](/images/openshift-project-installed-operators-admin.png)

* **B.** If you are in the `Developer` view, navigate to **+Add** -> **Operator backed**. This will show the list of all available operators. Make sure that you are in the correct `Project`.
![installed_operators](/images/openshift-project-installed-operators-developer.png)

Search for `GitLab Pages Site Operator` and click on it. This will open the operator description, similar to the following image:

![operator](/images/openshift-project-delete-site-operator.png)

Go to the **Publish a static site from a GitLab repository (gitlab.cern.ch)** tab in the top bar as you can see in the previous image. This will show the list of your sites created in this project. Search for the `GitlabPagesSites` site that you want to remove and click on the three dots to select the `Delete GitlabPagesSite` option.

![delete_operator](/images/openshift-project-edit-site-operator.png)

This will open a confirmation message. Click on the red `Delete` button and your `GitlabPagesSite` site will be removed.

This action will just remove the `GitlabPagesSite`'s webeos configurations and resources for this site, so in case that you did not remove your GitLab Pages resources, you will be able to recreate your site again by clicking on the `Create GitlabPagesSites` button in the top-right part and filling the form again

!!! warning
    You can use different values for the `Name`, `Labels` and `Anonymous`, however, `Host` has to be the same as your previous site, since has to be the same as your GitLab Pages domain.