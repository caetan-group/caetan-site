# Create a new site on Web Services

The first thing you want to do is create a new site on Web Services to eventually host your static documentation site.

#### A. Go to the [**Create new site**](https://webservices.web.cern.ch/webservices/Services/CreateNewSite/Default.aspx) page

#### B. Fill in the information

* **Site category**: Select "Official website".
* **Site name**: Enter the name of your site. For example "service-docs". This will form the default URL of your site (in this example `http://service-docs.web.cern.ch`). In a later step you will be able to change the URL to the shorter and more domain-specific `http://service.docs.cern.ch`.

!!! tip "Keep in mind"
    It is important that your site name is of the "service-docs" format, i.e. ending in "-docs". In the current implementation of the Web Services the site name forms the default URL of your site. In the future, Web Services will offer more flexible options for site URLs. It will then be much easier to internally migrate your site to the appropriate format if it's recognisable as a documentation site via the "-docs" suffix.

* **Description**: Enter a description of your site. For example "*My service name* documentation". This will help you and other users remember what this site is about.
* **Site type**: Select "PaaS Web Application". Thus will automatically create a new OpenShift project for you.
* **CERN Computing Rules**, **design guidelines** and **website lifecycle policy**: Read, make sure you agree and check it <input type="checkbox" checked="checked">.

![Screenshot](/images/web_services-create_new_site-filled_in.png)

---

#### C. Click on **Create new website**

![Screenshot](/images/web_services-create_new_site-filled_in-do.png)

---

Your new site will be ready shortly (and you will be notified by e-mail), in the meantime you can move on to the next step and [create a new git repository](new_repository.md).
