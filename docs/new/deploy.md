# Deploy your documentation

It's now to time to actually deploy your documentation. To follow this step [your site must have already been fully created](new_site.md).

#### A. Go to your site management on Web Services

Go to the [My websites](https://webservices.web.cern.ch/webservices/Services/ManageSite/) page and click on your site under "My websites" on the left. This is essentially the URL of your site (for example `http://cern.ch/service-docs`).

![Screenshot](/images/web_services-my_websites-do.png)

---

![Screenshot](/images/web_services-site_management.png)

---

#### B. Go to your OpenShift project

Click on **Manage your site** under "OpenShift application tools" on the left. Your OpenShift project was created automatically when you created your site as a "PaaS Web Application".

![Screenshot](/images/web_services-site_management-openshift_manage-do.png)

---

![Screenshot](/images/openshift-project-blank.png)

---

#### C. Browse the Catalog

Click on **Browse Catalog**.

![Screenshot](/images/openshift-project-browse_catalog-do.png)

---

![Screenshot](/images/openshift-project-browse_catalog.png)

---

#### D. Select MkDocs

Click on **MkDocs**.

![Screenshot](/images/openshift-project-browse_catalog-mkdocs-do.png)

---

![Screenshot](/images/openshift-project-browse_catalog-mkdocs-selected.png)

---

Click on **Next**.

![Screenshot](/images/openshift-project-browse_catalog-mkdocs-selected-do.png)

---

Select the most recent **Version** number from the menu below. 

![Screenshot](/images/openshift-project-browse_catalog-mkdocs-blank.png)

---

#### E. Fill in the information

* **Version**: Select the most recent version number in the menu.
* **Application Name**: Enter a name for your OpenShift application. For example "docs". This is an arbitrary name and does not affect at all the URL of your documentation site.
* **Git Repository**: Enter the HTTPS clone URL of your git repository, including the deploy token you generated in the [previous step](deploy_token.md). For example, you can construct the value as follows:
    * Start from your HTTPS clone URL: `https://gitlab.cern.ch/service/docs.git`.
    * Split the HTTPS clone URL in 2 parts:
        * the protocol `https://` and
        * the domain `gitlab.cern.ch/service/docs.git`.
    * Join the deploy token username `gitlab+deploy-token-***` and the deploy token itself with a colon symbol (`:`) to form the credentials: `gitlab+deploy-token-***:********`.
    * Add the credentials between the protocol and the domain to construct the final value:

        ```
        https://gitlab+deploy-token-***:********@gitlab.cern.ch/service/docs.git
        ```

!!! tip
    You can easily get the HTTPS clone URL of your git repository by going to your git repository on GitLab, clicking on **Clone** and copying the "Clone with HTTPS" URL.

    ![Screenshot](/images/gitlab-clone_https.png)

![Screenshot](/images/openshift-project-browse_catalog-mkdocs-filled_in.png)

---

#### F. Click on **Create**

![Screenshot](/images/openshift-project-browse_catalog-mkdocs-filled_in-do.png)

---

Your documentation is now being deployed and it will be available on the web in a few seconds or minutes. You can move on to the next step and [customise your documentation URL](route.md).
